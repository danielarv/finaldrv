package deloitte.academy.lesson01.run;

import java.io.InputStream;
import java.util.Scanner;
import java.util.logging.Logger;
import deloitte.academy.lesson01.machine.MetodosArticulos;

/**
 * Clase principal para ejecutar los metodos de la maquina expendedora.
 * 
 * @author dareyes@externodeloittemx.com
 * @version 1.0
 * @since 2020-03-10
 */

public class Ejecutar {

	/**
	 * Instruccion para poder insertar datos desde consola.
	 */
	private static Scanner scanner;

	/**
	 * Clase principal para ejecutar metodos.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		final Logger LOGGER = Logger.getLogger(Ejecutar.class.getName());
	

		// Creacion de objetos para registrar articulos
		/*
		MetodosArticulos.createProducto("A1", "Chocolate", 10.5, 10);
		MetodosArticulos.createProducto("A2", "Doritos", 15.5, 4);
		MetodosArticulos.icreateProducto("A3", "Coca", 22.5, 2);
		MetodosArticulos.icreateProducto("A4", "Gomitas", 8.75, 6);
		MetodosArticulos.icreateProducto("A5", "Chips", 30.0, 10);
		MetodosArticulos.icreateProducto("A6", "Jugo", 15.0, 2);
		MetodosArticulos.icreateProducto("B1", "Galletas", 10.0, 3);
		MetodosArticulos.icreateProducto("B2", "Canelitas", 120.0, 6);
		MetodosArticulos.icreateProducto("B3", "Halls", 10.1, 10);
		MetodosArticulos.icreateProducto("B4", "Tarta", 3.14, 10);
		MetodosArticulos.icreateProducto("B5", "Sabritas", 15.55, 0);
		MetodosArticulos.icreateProducto("B6", "Cheetos", 12.25, 4);
		MetodosArticulos.icreateProducto("C1", "Rocaleta", 10.0, 1);
		MetodosArticulos.icreateProducto("C2", "Rancheritos", 14.75, 6);
		MetodosArticulos.icreateProducto("C3", "Ruffles", 13.15, 10);
		MetodosArticulos.icreateProducto("C4", "Pizza fria", 22.0, 9);
		*/

		// Creacion de nuevo producto desde consola
		InputStream stream = System.in;
		scanner = new Scanner(stream);
		System.out.println("Ingresa el codigo del producto: ");
		String cod = scanner.nextLine();

		System.out.println("Ingresa el nombre del producto: ");
		String art = scanner.nextLine();

		System.out.println("Ingresa el precio del producto: ");
		double price = scanner.nextDouble();

		System.out.println("Ingresa la cantidad del producto: ");
		int cant = scanner.nextInt();

		// Agregar producto al objeto "almacen"
		// MetodosArticulos.icreateProducto(cod, art, price, cant);

		// Validacion de existencia de codigo
		String[] codigos = { "A1", "A2", "A3", "A4", "A5", "A6", "B1", "B2", "B3", "B3", "B4", "B5", "B6", "C1", "C2",
				"C3", "C4" };

		// Validar existencia de codigo

		System.out.println("Ingresa el codigo del producto que quieres comprar: ");
		String buscarCodigo = scanner.nextLine();

		String mensaje = "";
		for (String elemento : codigos) {
			if (buscarCodigo.equals(elemento)) {

				mensaje = "El producto se encuentra disponible.";
				break;

			} else {
				mensaje = "Por el momento el producto no est� disponible. Elige otro.";
			}
		}
		LOGGER.info(mensaje + " El codigo que buscaste es: " + buscarCodigo);

		MetodosArticulos ma = new MetodosArticulos(mensaje, mensaje, price, cant);

		// Mandar llamar producto para actualizar la cantidad de articulos en stock
		int actualizacion = ma.updateProducto("B5", 7);
		System.out.println("\nLa nueva cantidad de articulos de este producto es: " + actualizacion);

		// Mandar llamar producto para eliminarlo de la lista
		String delete = MetodosArticulos.deleteProducto("C4");
		System.out.println("\nCodigo a borrar: " + delete);

		// Mandar llamar producto para vender producto
		String total = MetodosArticulos.ventasDia();
		System.out.println(total);

	}
}