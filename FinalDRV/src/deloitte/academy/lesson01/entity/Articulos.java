package deloitte.academy.lesson01.entity;

import java.util.ArrayList;
import deloitte.academy.lesson01.machine.MetodosArticulos;

/**
 * La clase Articulos contiene todos los atributos que cada articulo debe tener.
 * Se trabajan los constructores, getters and setters para poderlos mandar.
 * llamar en la Clase Main.
 * 
 * @author dareyes@externosdeloittemx.com
 * @version 1.0
 * @since 2020-03-10 
 *
 */
public abstract class Articulos {

	// Creacion de lista para guardar los objetos de los articulos creados.
	public static ArrayList<Articulos> productos = new ArrayList<Articulos>();

	// Declaracion de atributos
	private String codigo;
	private String articulo;
	private double precio;
	private int cantidad;
	private int vendidos;


	// Constructores.
	/**
	 * Constructor de la clase Articulos. Permite crear objetos y heredar atributos.
	 */
	public Articulos() {

	}

	/**
	 * El constructor permite agregar atributos a los objetos creados de la clase
	 * Articulos.
	 * 
	 * @param codigo   Debe ser de tipo String.
	 * @param articulo Debe ser de tipo String.
	 * @param precio   Debe ser de tipo Double.
	 * @param cantidad Debe ser de tipo Int.
	 */
	public Articulos(String codigo, String articulo, double precio, int cantidad) {
		super();
		this.codigo = codigo;
		this.articulo = articulo;
		this.precio = precio;
		this.cantidad = cantidad;
		// Agregar todos los registros a la lista.
		productos.add(this);
	}

	// Getters and Setters de los Atributos de la Clase Articulos.

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getArticulo() {
		return articulo;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public int getVendidos() {
		return vendidos;
	}

	public void setVendidos(int vendidos) {
		this.vendidos = vendidos;
	}

	// Metodos que va a heredar la clase MetodosArticulos.

	/**
	 * Metodo para vender un producto y agregarlo a una lista de ventas del dia.
	 * @param codigo Debe ser de tipo String.
	 */
	public void vender(String codigo) {

	}

	/**
	 * Metodo para imprimir las descripciones de los atributos.
	 * 
	 * @return Retorna un valor de tipo String.
	 */
	public String Impresion() {

		return null;
	}

	/**
	 * Metodo que registra cuantas ventas se hicieron al dia.
	 * 
	 * @return Retorna un valor de tipo String.
	 */
	public static String ventasDia() {
		return null;
	}

	/**
	 * Metodo para actualizar la cantidad de articulos de un producto dentro de la
	 * maquina expendedora.
	 * 
	 * @param codigo Debe ser de tipo String.
	 * @param update Debe ser de tipo Int.
	 * @return Retorna un valor de tipo Int.
	 */
	public int updateProducto(String codigo, int update) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * Metodo para borrar un producto de la lista.
	 * 
	 * @param cod Debe ser de tipo String.
	 * @return Retorna un valor de tipo String.
	 */
	public static String deleteProducto(String cod) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Metodo para registrar un nuevo producto.
	 * 
	 * @param codigo   Debe ser de tipo String.
	 * @param articulo Debe ser de tipo String.
	 * @param precio   Debe ser de tipo Double.
	 * @param cantidad Debe ser de tipo Int.
	 */
	public abstract void icreateProducto(String codigo, String articulo, double precio, int cantidad);

	/**
	 * Metodo para leer los datos del producto.
	 * 
	 * @param producto Objeto a leer.
	 */
	public void ireadProducto(MetodosArticulos producto) {
	}
	
	/*
	 * @deprecated
	 */
	public abstract String toString();

	public void createProducto1(String codigo, String articulo, double precio, int cantidad) {
		// TODO Auto-generated method stub
		
	}

}
