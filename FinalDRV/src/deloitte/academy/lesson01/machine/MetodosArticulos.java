package deloitte.academy.lesson01.machine;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import deloitte.academy.lesson01.entity.Articulos;

/**
 * Clase donde se presentan todos los metodos para ejecutar en Main Class.
 * 
 * @author dareyes@externosdeloittemx.com
 * @version 1.0
 * @since 2020-03-10
 * {@inheritDoc}
 */
public class MetodosArticulos extends Articulos {

	private static final Logger LOGGER = Logger.getLogger(MetodosArticulos.class.getName());;

	// Lista para almacenar productos vendidos.
	static ArrayList<MetodosArticulos> productosVendidos = new ArrayList<MetodosArticulos>();

	// Constructores heredados de la superClase Articulos.
	public MetodosArticulos(MetodosArticulos metodosArticulos) {
		super();

	}

	/**
	 * Constructor para heredar los atributos a cualquier objeto de la clase
	 * Articulos, MetodosArticulos.
	 * 
	 * @param codigo
	 * @param articulo
	 * @param precio
	 * @param cantidad
	 */
	public MetodosArticulos(String codigo, String articulo, double precio, int cantidad) {
		super(codigo, articulo, precio, cantidad);

	}

	// Metodos CRUD

	/**
	 * El metodo permite registrar un nuevo producto e imprime un texto de que fue
	 * correctamente registrado.
	 */
	public void icreateProducto(String codigo, String articulo, double precio, int cantidad) {
		new MetodosArticulos(codigo, articulo, precio, cantidad);
		LOGGER.info("PRODUCTO REGISTRADO CORRECTAMENTE.");

	}

	/**
	 * Esta clase busca el codigo del producto dentro de la lista principal y
	 * actualiza la cantidad.
	 * @exception Si el codigo no se encuentra dentro de la lista, muestra un error.
	 * @throws Error.
	 * @serial Recorre la lista de articulo en articulo de los datos registrados.
	 */
	public int updateProducto(String codigo, int update) {
		int actualizacion = 0;
		try {
			for (Articulos artic : Articulos.productos) {
				if (artic.getCodigo().equals(codigo)) {
					int aux = this.getCantidad();
					actualizacion = aux + update;
					LOGGER.info("PRODUCTO ACTUALIZADO CORRECTAMENTE");
				}
			}
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Error. ", ex);
		}
		return actualizacion;

	}

	/**
	 * El metodo permite buscar un producto por Codigo y borrarlo los datos de
	 * lista. Imprime el valor del codigo que fue eliminado.
	 * 
	 * @param cod Valor de tipo String.
	 * @return Retorna un mensaje (String).
	 * @exception Si el codigo no se encuentra dentro de la lista, muestra un error.
	 * @throws Error.
	 */
	public static String deleteProducto(String cod) {
		String message = " ";
		try {
			for (Articulos artic : Articulos.productos) {

				if (artic.getCodigo().equals(cod)) {
					Articulos.productos.remove(artic);
					message = "El art�culo con c�digo: " + cod + " ha sido eliminado.";
					break;
				}
			}
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Error. ", ex);
		}
		return message;

	}

	/**
	 * Imprimite un formato de acuerdo al Metodo Impresion.
	 */
	public void ireadProducto(MetodosArticulos producto) {
		LOGGER.info(this.Impresion());

	}

	/**
	 * El metodo imprime el formato para que aparezca los datos en consola.
	 * 
	 */
	@Override
	public String Impresion() {
		String status = "Codigo: " + this.getCodigo() + " Articulo: " + this.getArticulo() + " Precio: "
				+ this.getPrecio() + " Stock: " + this.getCantidad();

		return status;

	}

	/**
	 * El metodo permite buscar un producto por su codigo y agregarlo a la lista de
	 * vendidos. Si el producto no se encuentra dentro de la lista, aparece un texto
	 * de producto agotado.
	 * 
	 * @param codigo Debe ser de tipo String.
	 * @exception Si el codigo no se encuentra dentro de la lista, muestra un error.
	 * @throws Error.
	 */
	@Override
	public void vender(String codigo) {
		try {
			if (Articulos.productos.contains(this)) {
				MetodosArticulos aux = new MetodosArticulos(this);
				this.setCantidad(this.getCantidad() - 1);
				aux.setCantidad(1);
				productosVendidos.add(aux);
			} else {
				LOGGER.info("PRODUCTO AGOTADO.");
			}
		} catch (Exception e) {
			LOGGER.severe("Error " + e);

		}

	}

	/**
	 * Metodo que permite buscar un articulo dentro de una lista e imprimir cuantos
	 * articulos y cu�nto se vendi�.
	 * 
	 * @return Retorna un valor de tipo String.
	 * @exception Si el codigo no se encuentra dentro de la lista, muestra un error.
	 * @throws Error.
	 */
	public static String ventasDia() {
		String ventas = "";
		try {
			for (Articulos artic : productosVendidos) {
				ventas += artic.getArticulo() + "Productos vendidos: " + artic.getVendidos() + " Total: $"
						+ artic.getVendidos() * artic.getPrecio() + "\n";
			}
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Error. ", ex);
		}

		return ventas;

	}

	@Override
	/**
	 * @deprecated Should no longer be used.
	 */
	/*
	public static void createProducto1(String codigo, String articulo, double precio, int cantidad) {
		// TODO Auto-generated method stub
		
	}

	@Override
	/**
	 * @deprecated Should no longer be used.
	 */
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}

}